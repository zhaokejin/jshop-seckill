package com.sugo.seckill.web.order;

import com.sugo.seckill.http.HttpResult;
import com.sugo.seckill.http.HttpStatus;
import com.sugo.seckill.order.service.SeckillOrderService;
import com.sugo.seckill.pojo.FrontUser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.*;


/**
 * controller
 * @author Administrator
 *
 */
@RestController
@RequestMapping("/seckill")
public class SeckillOrderController {

	@Autowired
	private SeckillOrderService seckillOrderService;


			/**
             * @Description: 获取时间
             * @Author: hubin
             * @CreateDate: 2020/6/10 16:19
             * @UpdateUser: hubin
             * @UpdateDate: 2020/6/10 16:19
             * @UpdateRemark: 修改内容
             * @Version: 1.0
             */
	@RequestMapping("/submitOrder/times")
	public HttpResult getConcurrentTime(){
		return HttpResult.ok(System.currentTimeMillis()+"");
	}


	@RequestMapping("/test")
	public HttpResult getSeckillGoods(Long seckillId){
		seckillOrderService.getSeckillGoods(seckillId);
		return HttpResult.ok();
	}

	/**
	 * @Description: 普通下单操作
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@RequestMapping("/order/kill/{killId}/{token}")
	public HttpResult startKilled(@PathVariable Long killId, @PathVariable String token){
		//判断
		if(StringUtils.isBlank(token)){
			return HttpResult.error(HttpStatus.SC_EXPECTATION_FAILED,"用户未登录");
		}
		//获取用户数据
		FrontUser user = seckillOrderService.getUserInfoFromRedis(token);

		//判断
		if(user == null){
			return HttpResult.error(HttpStatus.SC_EXPECTATION_FAILED,"用户未登录");
		}

		//获取userid
		String userId = user.getId()+"";
		//下单
		HttpResult result = seckillOrderService.startKilled(killId, userId);

		return result;

	}

}
